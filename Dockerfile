FROM ruby:2.7.5-alpine3.15

# Install git because it is a dependency
RUN apk update
RUN apk add git

RUN gem install gfsm

COPY ./gfsmrc.yml ./gfsmrc.yml

ENTRYPOINT [""]
